#include<iostream>
#include<string>
#include<ctype.h>
int size_of_matrix;

int check_input(std::string input) {

	int index, flag = 0, check_dot = 0,negative_check=0;

	for (index = 0; index <input.length(); index++)
	{

		if ((index == 0)&&(input[0] == '-'))
		{
			negative_check = 1;
		}
		 else if ((input[index] == '.') )
		{
			check_dot++;	

		}
		else if (isdigit(input[index]) == 0)             
		{
			flag = 1;
			break;
		}

	}

	if ((flag == 0)&&(check_dot <= 1) &&(negative_check<=1))
		return 1;

	return 0;
}

class Matrix {

public:
	float **elements;
	Matrix(int size_of_matrix);
	void GetElements();
	void PrintElements();

	Matrix operator +(Matrix const &object) {

		int index1, index2;
		Matrix matrix3(size_of_matrix);
		for (index1 = 0; index1 < size_of_matrix; index1++)
			for (index2 = 0; index2 < size_of_matrix; index2++)
				matrix3.elements[index1][index2] = elements[index1][index2] + object.elements[index1][index2];

		return matrix3;
	}

	Matrix operator *(Matrix const &object) {

		int index1, index2, index3, sum ;
		Matrix matrix3(size_of_matrix);
		for (index1 = 0; index1 < size_of_matrix; index1++)
		{
			for (index2 = 0; index2 < size_of_matrix; index2++)
			{
				sum = 0;
				for (index3 = 0; index3 < size_of_matrix; index3++)
					sum = sum + elements[index1][index3] * object.elements[index3][index2];

				matrix3.elements[index1][index2] = sum;
			}
		}
		return matrix3;
	}
};

Matrix::Matrix(int size_of_matrix) {

	elements = new float *[size_of_matrix];
	int index;

	for (index = 0; index < size_of_matrix; index++)
		elements[index] = new float[size_of_matrix];
}

void Matrix::GetElements()
{
	int row,column;
	std::string input;
	for (row = 0; row < size_of_matrix; row++)
	{
		for (column = 0; column < size_of_matrix; column++)
		{
			std::cin >> input;
			if (check_input(input) == 0)
			{
				std::cout << "You have entered wrong input";
				exit(0);
			}
			elements[row][column] = std::stof(input);   //changing string to float
		}
	}
}

void Matrix::PrintElements() {

	int row, column;
	
	for (row = 0; row < size_of_matrix; row++)
	{
		for (column = 0; column < size_of_matrix; column++)
		{
			std::cout << elements[row][column] << " ";
		}
		std::cout << std::endl;
	}
}

int main() {
	
	std::cout << "Enter the size of matrix ";
	std::cin >> size_of_matrix;

	if (size_of_matrix <=0)
	{
		std::cout << "You have entered wrong input";
		exit(0);
	}
	    Matrix matrix1(size_of_matrix);
		std::cout << "Enter the Elements of 1st matrix \n";
		matrix1.GetElements();
	

		Matrix matrix2(size_of_matrix);
		std::cout << "Enter the Elements of 2nd matrix \n";
		matrix2.GetElements();

		Matrix matrix3(size_of_matrix);
		
		std::cout << "Addition of Matrix :- \n";
		matrix3 = matrix1 + matrix2;
		matrix3.PrintElements();

		std::cout << "Multiplication of Matrix :- \n";
		matrix3 = matrix1 * matrix2;
		matrix3.PrintElements();

		return 0;
}