#include<iostream>

class Copy_Constructor{
public:
	int number1, number2;
	Copy_Constructor(int x,int y) {
		number1 = x;
		number2 = y;
	 }
	Copy_Constructor(Copy_Constructor &object) {
		number1 = object.number1;
		number2 = object.number2;
	}
	void print() {
		std::cout << "Number1 is " << number1 << " and Number2 is " << number2 << std::endl;
	}

};
int main() {

	Copy_Constructor object(4,5);
	Copy_Constructor object1 = object;
	object.print();
	object1.print();
	return 0;

}