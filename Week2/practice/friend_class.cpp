#include<iostream>
using namespace std;
class base {
public:
	int number1,number2;
	base(int x,int y) {
		number1 = x;
		number2 = y;
	}
private:
	friend class subclass;
};
class subclass {
public:
	void display(base object) {
		std::cout << "Value of number1 is " << object.number1 <<std::endl;
		std::cout << "Value of number2 is " << object.number2 <<std::endl;
	}
};
int main() {
	base object(1, 2);
	subclass object1;
	object1.display(object);





	return 0;
}