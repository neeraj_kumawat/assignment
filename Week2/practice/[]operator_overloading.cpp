#include<iostream>
class Student {
public:
	int subjects[3];
	Student(int marks1, int marks2, int marks3) {
		subjects[0] = marks1;
		subjects[1] = marks2;
		subjects[2] = marks3;
	}
	int operator [](int position) {

		return subjects[position];
	}
};
int main() {
	
	Student student(50, 60, 70);
	std::cout << student[0] << std::endl;
	std::cout << student[1] << std::endl;
	std::cout << student[2] << std::endl;


	return 0;
}