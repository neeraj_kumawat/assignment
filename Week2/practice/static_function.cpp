#include<iostream>

class test {
public:
	int x;
	static int number;
	static void print(test object) {
		std::cout << object.x;
	}
};

int test::number = 0;
int main() {
	
	test object;
	object.x = 0;
	test::print(object);

	return 0;
}