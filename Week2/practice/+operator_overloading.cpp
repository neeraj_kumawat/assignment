#include<iostream>

class Student {
public:
	int marks;
	Student(int marks) {
		this->marks = marks;
	}
	Student() {
		marks = 0;
	}
	Student operator +(Student const &student) {
		Student total;
		total.marks = marks + student.marks;
		return total;
	}
};
int main() {
	Student student1(100);
	Student student2(200);
	Student student3 = student1 + student2;
       std::cout << "Total marks is " << student3.marks;


	return 0;
}