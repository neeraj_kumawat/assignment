#include<iostream>

class abstract {
public:
	virtual void print()=0;
};
class derived :public abstract{
public:
	void print() {
		std::cout << "It is derived class";
	}
};
int main() {
	derived object;
	object.print();

	return 0;
}