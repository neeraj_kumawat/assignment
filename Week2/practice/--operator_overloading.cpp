#include<iostream>

class Student {
public:
	int marks;
	Student(int m) {
		marks = m;
	}

	void operator --() {
		marks = marks - 1;
	}
};

int main() {
	Student student(100);
	std::cout << "Before Increment marks is " << student.marks<<std::endl;
	--student;
	std::cout << "After Increment marks is " << student.marks;




	return 0;
}