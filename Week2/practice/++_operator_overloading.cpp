#include<iostream>

class Student {
public:
	int marks;

	Student(int number) {
		marks = number;
	}

	void print_marks() {
		std::cout << "Marks is " << marks;
	}

	void operator ++() {
		++marks;
	}

	void operator ++(int) {
		marks++;
	}

};

int main() {

	Student student(50);

	std::cout << "Before Increment " << student.marks <<"\n";
	++student;
	std::cout << "After pre Increment " << student.marks<<"\n";
	student++;
	std::cout << "After post Increment " << student.marks <<"\n";



}