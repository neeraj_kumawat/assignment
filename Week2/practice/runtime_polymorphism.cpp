#include<iostream>


class Subclass {
public:
	virtual void show() {
		std::cout << "Base class is called";
	}
};

class Derived :public Subclass {
public:
	void show() {                     
		std::cout << "Derived class is called";
	}
};
int main() {

	Subclass *base;
	Derived derived;
	base = &derived;

	base->show();


	return 0;
}