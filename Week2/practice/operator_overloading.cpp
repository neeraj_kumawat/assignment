#include<iostream>
class operator_overloading {
public:
	int number;
	friend std::istream& operator >> (std::istream &input, operator_overloading &object);
	friend std::ostream& operator << (std::ostream &output, operator_overloading &object);
};

std::istream &operator >> (std::istream &input, operator_overloading &object) {
	input >> object.number;
	return input;
}

std::ostream &operator << (std::ostream &output, operator_overloading &object) {
	output << object.number;
	return output;
}

int main() {
	operator_overloading object;
	std::cin>> object;
	std::cout << object;
	


	return 0;
}