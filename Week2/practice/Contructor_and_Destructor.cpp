#include <iostream>

class Constructor {
public:
	Constructor() {
		std::cout << "Object is created"<<std::endl;
	}
	~Constructor() {
		std::cout << "Object is destroyed"<<std::endl;
	}
	Constructor(int number) {
		std::cout << "Argument passed is " << number <<std::endl;
	}
};
int main()
{
	Constructor object;

	Constructor object1(10);

	return 0;
	
}

