#include<iostream>
#include<mutex>
#include<thread>

std::mutex m1, m2;

void thread1()
{
	m1.lock();
	m2.lock();
	std::cout << "Criticial Section of Thread 1 \n";
	m1.unlock();
	m2.unlock();
}

void thread2() {

	m2.lock();
	m1.lock();
	std::cout << "Criticial Section of Thread 2 \n";
	m2.unlock();
	m1.unlock();
}

int main()
{
	std::thread object1(thread1);
	std::thread object2(thread2);

	object1.join();
	object2.join();

	return 0;
}