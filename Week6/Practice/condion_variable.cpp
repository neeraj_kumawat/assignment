#include<iostream>
#include<mutex>
#include<thread>

int amount = 0;
std::mutex m;
std::condition_variable cv;

void add_money(int money)
{
	std::unique_lock<std::mutex> lock1(m);

	amount += money;
	std::cout << "Amount is added Current balance is " << amount<<std::endl;
	cv.notify_one();

}

void withdraw_money(int money)
{
	std::unique_lock<std::mutex> lock2(m);
	cv.wait(lock2, [] {return amount != 0 ? true : false; });
	if (amount <= money)
	{
		amount -= money;
		std::cout << "Amount is deducted \n";
	}
	else
	{
		std::cout << "Amount cannot be deducted beause of insufficient balance \n";
	}
	std::cout << " Current balance is " << amount;

}

int main()
{
	std::thread thread1(withdraw_money, 10);
	std::thread thread2(add_money, 10);


	thread1.join();
	thread2.join();

	std::cout << amount;

	return 0;
}