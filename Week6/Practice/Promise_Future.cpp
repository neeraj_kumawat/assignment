#include<iostream>
#include<mutex>
#include<thread>
#include<future>

void factorial(std::promise <int > &  promise ,int number) {             

	int factorial = 1;

	for (int index = 1; index <= number; index++)
	{
		factorial = factorial * index;
	}
	promise.set_value(factorial);
}

int main() {

	std::promise <int > promise;
	std::future <int > future = promise.get_future();

	std::thread thread(factorial, std::ref(promise), 5);

	std::cout << "Waiting for result \n";

	std::cout << "Factorial is " << future.get();

	thread.join();

	return 0;
}