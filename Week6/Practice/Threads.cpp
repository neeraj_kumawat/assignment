#include<iostream>
#include<thread>
#include<chrono>
#include<mutex>
/*
void increment (int count)
{
	for (int index = 1; index <= count; index++)
	{
		std::cout << index << " ";
	}
	std::cout << " \n";
	std::this_thread::sleep_for(std::chrono::seconds(5));
}
*/
int amount = 0;
std::mutex m;

void addmoney(int money)
{
	m.lock();
	
	++amount;

	m.unlock();
}

int main()
{
	//std::thread thread(increment,10);

	//thread.join();
	
	std::thread thread1(addmoney, 10);
	std::thread thread2(addmoney, 20);
	std::thread thread3(addmoney, 20);
	std::thread thread4(addmoney, 20);

	thread1.join();
	thread2.join();
	thread3.join();
	thread4.join();

	std::cout << amount;
	return 0;
}