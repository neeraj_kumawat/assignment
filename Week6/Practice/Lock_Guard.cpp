#include<iostream>
#include<mutex>
#include<thread>

int amount = 0;
std::mutex m;

void increment(int value)
{
	std::lock_guard<std::mutex> lock(m);

		amount++;
	
     
}

int main()
{
	std::thread thread1(increment, 10);
	std::thread thread2(increment, 10);


	thread1.join();
	thread2.join();

	std::cout << amount;

	return 0;
}