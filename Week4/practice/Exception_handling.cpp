#include<iostream>

int main() {

	int amount;

	std::cout << "Enter the amount you want to deposit into the bank \n";
	std::cin >> amount;

	try {
		
		if (amount > 0)
		{
			std::cout << "Your amount " << amount << " is deposited successfully \n";
		}
		else
		{
			throw amount;
		}

	}
	catch (int amount)
	{
		std::cout << "You have entered wrong value please try again ...\n";
	} 

	return 0;
}