#include<iostream>
#include<queue>

void Display(std::queue<int> queue)
{
	std::cout << "Elements are ";

	while(!queue.empty())
	{
		std::cout << queue.front()<<" ";
		queue.pop();
	}

	std::cout << std::endl;
}

int main() {


	std::queue <int> queue;
	int option, check = 1, element;

	while (check == 1)
	{

		std::cout << "Enter 1 to check for Queue is empty or not \n";
		std::cout << "Enter 2 for push a element \n";
		std::cout << "Enter 3 for pop a element from Queue \n";
		std::cout << "Enter 4 to Display Queue front element \n";
		std::cout << "Enter 5 to Display Queue back element \n";
		std::cout << "Enter 6 to display the queue element \n";
		std::cin >> option;

		switch (option)
		{
		case 1:
			if (queue.empty())
				std::cout << "Queue is empty \n";
			else
				std::cout << "Queue is not empty \n";
			break;

		case 2:
			std::cout << "Enter a element to push \n";
			std::cin >> element;
			queue.push(element);
			break;

		case 3:
			if (queue.empty())
				std::cout << "Queue is empty \n";
			else
				queue.pop();
			break;

		case 4:
			if (queue.empty())
				std::cout << "Queue is empty \n";
			else
				std::cout << queue.front() << "\n ";
			break;

		case 5:
			if (queue.empty())
				std::cout << "Queue is empty \n";
			else
				std::cout << queue.back() << "\n ";
			break;

		case 6:
			if (queue.empty())
				std::cout << "Queue is empty \n";
			else
				Display(queue);
			break;

		default:
			std::cout << "Enter a valid input \n";
			break;
		}
	}

	return 0;
}