#include<iostream>
#include<vector>
#include<string>

int main() {

	std::vector <std::string> elements;
	
	std::cout << "Enter the name of students \n";

	int flag = 0;
	std::string name;
	
	do
	{

		std::cin >> name;
		elements.push_back(name);

		std::cout << "Enter 1 to stop entering names otherwise press 0 \n";
		std::cin >> flag;

	} while (flag == 0);

	for (int index = 0; index < elements.size(); index++)
	{
		std::cout << elements[index] << " \n";
	}

	return 0;
}