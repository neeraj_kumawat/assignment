#include<iostream>
#include<utility>

class Student {

public:
	int marks, roll_number;

	Student(int marks,int roll_number) {

		this->marks = marks;
		this->roll_number = roll_number;
	}

};
int main() {

	Student object(50,1);

	std::pair<int, int> student;

	student = std::make_pair(object.marks, object.roll_number);

	std::cout << "Student roll number is " << student.second << " \n";
	std::cout << "Student marks is " << student.first << " \n ";

	return 0;

}