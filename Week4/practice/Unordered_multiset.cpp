#include<iostream>
#include<unordered_set>

int main() {

	std::unordered_multiset<int> Multi_Set;
	int element;

	Multi_Set.insert(3);
	Multi_Set.insert(2);
	Multi_Set.insert(1);
	Multi_Set.insert(4);
	Multi_Set.insert(1);
	Multi_Set.insert(2);

	std::cout << "Printing All elements \n";
	for (auto index = Multi_Set.begin(); index != Multi_Set.end(); index++)
	{
		std::cout << *(index) << " ";
	}
	std::cout << std::endl;

	std::cout << "ENter the element you have delete \n";
	std::cin >> element;

	if (Multi_Set.count(element) == 0)
	{
		std::cout << "Element is not found \n";
	}
	else
	{
		Multi_Set.erase(element);
	}


	std::cout << "Printing All elements \n";
	for (auto index = Multi_Set.begin(); index != Multi_Set.end(); index++)
	{
		std::cout << *(index) << " ";
	}

	return 0;
}