#include<iostream>
#include<unordered_map>
#include<string>

int main() {

	std::unordered_map<std::string, int> data;
	std::string name;
	int age;

	data.insert(std::pair<std::string,int> ("neeraj", 20));
	data.insert(std::pair<std::string, int>("rahul", 18));
	data.insert(std::pair<std::string, int>("rohan", 19));
	data.insert(std::pair<std::string, int>("robin", 21));

	std::cout << "Enter the name for which you have to find age \n";
	std::cin >> name;

	if (data.find(name) == data.end())
	{
		std::cout << "Name is not found \n";
	}
	else
	{
		int age = data.at(name);

		std::cout << name << " age is " << age << " \n";
	}

	return 0;
}