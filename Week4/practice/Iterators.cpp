#include<iostream>
#include<vector>
#include<map>

int main() {

	std::vector<int> elements = { 1,2,3,4,5,6 };
	std::map<int,int> student;

	std::vector<int>::iterator traverse;

	for (traverse = elements.begin(); traverse != elements.end(); traverse++)
	{
		std::cout << *traverse << " ";
	}

	std::cout << std::endl;

	std::cout << "Now printing the value of map \n";

	student.insert(std::pair<int, int>(1, 89));
	student.insert(std::pair<int, int>(2, 90));
	student.insert(std::pair<int, int>(3, 91));
	student.insert(std::pair<int, int>(4, 92));

	std::map<int, int>::iterator index;

	for (index = student.begin(); index != student.end(); index++)
	{

		std::cout << "Roll no is " << index->first << " and  Marks is " << index->second<<" \n";
	}

	return 0;
}