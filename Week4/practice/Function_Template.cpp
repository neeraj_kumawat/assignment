#include<iostream>

template <class type1,class type2> 
void Addition(type1 number1,type2 number2)
{
	std::cout << "Addition of two numbers  " << number1 +number2 << std::endl;
}

int main() {

	Addition(4.4,5.4);
	Addition(4, 4.5);
	Addition(2, 2);

	return 0;
}