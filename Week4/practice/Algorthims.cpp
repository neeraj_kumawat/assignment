#include<iostream>
#include<vector>
#include<algorithm>
int main() {

	std::vector<int> data = { 8,7,6,65,5,53 };
	int element;

	std::cout << "Before sorting elements are \n";

	for (auto index = data.begin(); index != data.end(); index++)
	{
		std::cout << *index << " ";
	}

	std::cout << " \n After sorting \n";

	std::sort(data.begin(), data.end());

	for (auto index = data.begin(); index != data.end(); index++)
	{
		std::cout << *index << " ";
	}

	std::cout << " \n ENter a element to find  and count it \n";
	std::cin >> element;

	if (std::binary_search(data.begin(), data.end(), element))
	{
		std::cout << "Element is found \n";
		int count=std::count(data.begin(), data.end(), element);
		std::cout << element << " occurs  " << count << " times \n";
	}
	else
	{
		std::cout << "Element is not found \n";
	}


	

}
