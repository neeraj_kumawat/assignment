#include<iostream>
#include<queue>

void Display(std::priority_queue<int> priority_queue)
{
	std::cout << "Elements are ";

	while (!priority_queue.empty())
	{
		std::cout << priority_queue.top() << " ";
		priority_queue.pop();
	}

	std::cout << std::endl;
}

int main() {


	std::priority_queue <int> priority_queue;
	int option, check = 1, element;

	while (check == 1)
	{

		std::cout << "Enter 1 to check for Priority Queue is empty or not \n";
		std::cout << "Enter 2 for push a element \n";
		std::cout << "Enter 3 for pop a element from Priority Queue \n";
		std::cout << "Enter 4 to Display Priority Queue top element \n";
		std::cout << "Enter 5 to display the Priority Queue elements \n";
		std::cin >> option;

		switch (option)
		{
		case 1:
			if (priority_queue.empty())
				std::cout << "Queue is empty \n";
			else
				std::cout << "Queue is not empty \n";
			break;

		case 2:
			std::cout << "Enter a element to push \n";
			std::cin >> element;
			priority_queue.push(element);
			break;

		case 3:
			if (priority_queue.empty())
				std::cout << "Queue is empty \n";
			else
				priority_queue.pop();
			break;

		case 4:
			if (priority_queue.empty())
				std::cout << "Queue is empty \n";
			else
				std::cout << priority_queue.top() << "\n ";
			break;

		case 5:
			if (priority_queue.empty())
				std::cout << "Queue is empty \n";
			else
				Display(priority_queue);
			break;

		default:
			std::cout << "Enter a valid input \n";
			break;
		}
	}

	return 0;
}