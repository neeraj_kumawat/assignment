#include<iostream>
#include<deque>

int main() {

	std::deque<int> Dequeue;
	int flag = 1,element;

	while (flag == 1)
	{

		std::cout << "Enter the element \n";
		std::cin >> element;
		Dequeue.push_back(element);

		std::cout << "DO you want to continue press 1 otherwise press 0 \n";
		std::cin >> flag;
	}
	
		std::cout << "Front element is " << Dequeue.front()<<"\n";
		std::cout << "End element is " << Dequeue.back() << "\n";
		std::cout << "ELement are \n";

		std::deque<int>::iterator traverse = Dequeue.begin();

		while (traverse != Dequeue.end())
		{
			std::cout << *traverse << "\n";
			traverse++;
		}	

	return 0;
}