#include<iostream>
#include<set>
#include<string>

class  Student {
public:
	std::string name;
	float marks;

	bool operator < (const Student &object) const
	{
		return marks < object.marks;
	}

};

int main() {

	std::set <Student> Set = {
		{"neeraj",76},
	    {"raju",44},
	    {"rahul",99}
	};

    
	for (auto index = Set.begin(); index != Set.end(); index++)
	{
		std::cout << index->name << " got " << index->marks<<" \n";
	}

	return 0;
}