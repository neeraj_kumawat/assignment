#include<iostream>
#include<stack>

int main() {


	std::stack <int> stack;
	int option, check = 1,element;

	while (check == 1)
	{

		std::cout << "Enter 1 to check for stack is empty or not \n";
		std::cout << "Enter 2 for push a element \n";
		std::cout << "Enter 3 for pop a element from stack \n";
		std::cout << "Enter 4 to Display stack top element \n";
		std::cin >> option;

		switch (option)
		{
		case 1:
			if (stack.empty())
				std::cout << "Stack is empty \n";
			else
				std::cout << "Stack is not empty \n";
			break;

		case 2:
			std::cout << "Enter a element to push \n";
			std::cin >> element;
			stack.push(element);
			break;

		case 3:
			if (stack.empty())
				std::cout << "Stack is empty \n";
			else
			stack.pop();
			break;

		case 4: 
			if (stack.empty())
				std::cout << "Stack is empty \n";
			else
				std::cout << stack.top() << "\n ";
			break;

		default:
			std::cout << "Enter a valid input \n";
			break;
		}
	}

	return 0;
}