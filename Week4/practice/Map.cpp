#include<iostream>
#include<map>
#include<string>

int main() {

	std::map<std::string,int> data;
	int student_size,marks;
	std::string name;

	std::cout << "Enter the no. of student there \n";
	std::cin >> student_size;

	std::cout << "Enter the student name and student marks \n";
	for (int index = 1; index <= student_size; index++)
	{
		
		std::cin >> name >> marks;

		data.insert(std::pair<std::string,int> (name,marks));
	}

	std::map<std::string, int > ::iterator traverse = data.begin();

	while (traverse != data.end())
	{

		std::cout << "Key is " << traverse->first << " " << " and Value is " << traverse->second<<" \n";
		
		traverse++;
	}


	return 0;

}