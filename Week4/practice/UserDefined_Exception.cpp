#include<iostream>
#include<exception>

class Divide_By_Zero :public std::exception {
	
public:

	const  char * what() const throw()
	{
		return "Divide by zero Exception \n";
	}
};

int main() {

	float number1, number2;
            
	std::cout << "Enter two number for division \n";
	std::cin >> number1 >> number2;

	try {

		if (number2 == 0)
		{
			Divide_By_Zero exception;
			throw exception;
		}
		std::cout << "Division of two numbers is " << number1 / number2;
	}

	catch (Divide_By_Zero  &exception)
	{
		std::cout << exception.what();
	}

	return 0;
}