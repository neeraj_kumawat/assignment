#include<iostream>

template <class type>
class Math {

public: 
	
	type addition(type number1,type number2)
	{
		return number1 + number2;
	}

	type multiplication(type number1,type number2)
	{
		return number1 * number2;
	}

};
int  main( ) {

	Math<int> object1;
	Math<float> object2;
	std::cout<<object1.addition(3,5)<<"\n";
	std::cout << object2.multiplication(4, 5)<<"\n";

	std::cout << object2.addition(4.5, 5.3)<<"\n";
	std::cout << object2.multiplication(1.2, 2.2);

	return 0;
}