#include <iostream>
#include <cmath>

float *Numbers_Input(int length) {

	float *input = new float [length];
	std::cout << "Enter the numbers";

		for (int index = 0; index < length; index++)
			std::cin >> input[index];

	return input;
}

void Addition()
{
	float number1, number2;
	int operand_size;
	float *input, answer;

	std::cout << "Enter the no. of operands" << std::endl;
	std::cin >> operand_size;
	input = Numbers_Input(operand_size);
	answer = input[0];

	for (int index = 1; index < operand_size; index++)
		answer = answer + input[index];

	std::cout << "Answer is " << answer <<std::endl;
	delete input;
}

void Subtraction()
{
	float *input, answer;
	int operand_size;

	std::cout << "Enter the no. of operands" << std::endl;
	std::cin >> operand_size;
	input = Numbers_Input(operand_size);
	answer = input[0];

	for (int index = 1; index < operand_size; index++)
		answer = answer - input[index];

	std::cout << "Answer is " << answer << std::endl;
	delete input;
}

void Multiplication()
{
	float *input, answer;
	int operand_size;

	std::cout << "Enter the no. of operands" << std::endl;
	std::cin >> operand_size;
	input = Numbers_Input(operand_size);
	answer = input[0];

	for (int index = 1; index < operand_size; index++)
		answer = answer * input[index];

	std::cout << "Answer is " << answer << std::endl;
	delete input;
}
void Division()
{
	float *input, answer;
	int operand_size,flag=0;

	std::cout << "Enter the no. of operands" << std::endl;
	std::cin >> operand_size;
	input = Numbers_Input(operand_size);
	answer = input[0];

	for (int index = 1; index < operand_size; index++)
	{
		try {
			if (input[index] == 0)
				throw "Divide by zero error";
			answer = answer / input[index];
		}
		catch (const char *error) {
			std::cout << "Exception Occured " << error << std::endl;
			flag = 1;
			break;
		}
	}
	if(flag==0)
	std::cout << "Answer is " << answer << std::endl;
	delete input;

}

void Square()
{
	float number;
	std::cout << "Enter the number \n";
	std::cin >> number;
	std::cout << "Answer is " << number*number << std::endl;
}

void SquareRoot()
{
	float number;
	std::cout << "Enter the number \n";
	std::cin >> number;
	std::cout << "Answer is " << sqrt(number) << std::endl;
}

void Cube()
{
	float number;
	std::cout << "Enter the number \n";
	std::cin >> number;
	std::cout << "Answer is " <<number*number*number<< std::endl;
}

void CubeRoot()
{
	float number;
	std::cout << "Enter the number \n";
	std::cin >> number;
	std::cout << "Answer is  " << cbrt(number) << std::endl;
}

void LogicalOr()
{
	int operand_size;
	float *input, answer;

	std::cout << "Enter the no. of operands" << std::endl;
	std::cin >> operand_size;
	input = Numbers_Input(operand_size);
	answer = input[0];

	for (int index = 1; index < operand_size; index++)
		answer = answer || input[index];

	std::cout << "Answer is " << answer << std::endl;
	delete input;
}

void LogicalAnd()
{
	int operand_size;
	float *input, answer;

	std::cout << "Enter the no. of operands" << std::endl;
	std::cin >> operand_size;
	input = Numbers_Input(operand_size);
	answer = input[0];

	for (int index = 1; index < operand_size; index++)
		answer = answer && input[index];

	std::cout << "Answer is " << answer << std::endl;
	delete input;
}

void LogicalNot()
{

	float number;
	std::cout << "Enter the number";
	std::cin >> number;
	std::cout << "Answer is " << !(number)<< std::endl;
}

void LogicalXor()
{
	
	int *input, answer,operand_size;

	std::cout << "Enter the no. of operands" << std::endl;
	std::cin >> operand_size;
	input = new int[operand_size];

	std::cout << "Enter the numbers";
	for (int index = 0; index < operand_size; index++)
		std::cin >> input[index];

	answer = input[0];
	for (int index = 1; index < operand_size; index++)
		answer = answer ^ input[index];

	std::cout << "Answer is " << answer << std::endl;
	delete input;
}

int main()
{
	int flag = 0;
	int option;
	while (flag == 0) {

	std::cout << "Enter 1  for addition \n";
	std::cout << "Enter 2  for subtraction \n";
	std::cout << "Enter 3  for Multiplication \n";
	std::cout << "Enter 4  for Division \n";
	std::cout << "Enter 5  for Square \n";
	std::cout << "Enter 6  for Square root \n";
	std::cout << "Enter 7  for Cube \n";
	std::cout << "Enter 8  for Cube root \n";
	std::cout << "Enter 9  for Logical OR \n";
	std::cout << "Enter 10 for Logical AND \n";
	std::cout << "Enter 11 for Logical NOT \n";
	std::cout << "Enter 12 for Logical XOR \n";
	std::cin >> option;

		switch (option) {
		case 1:
			Addition();
			break;

		case 2:
			Subtraction();
			break;
			

		case 3:
			Multiplication();
			break;

		case 4:
			Division();
			break;

		case 5:
			Square();
			break;

		case 6:
			SquareRoot();
			break;

		case 7:
			Cube();
			break;

		case 8:
			CubeRoot();
			break;

		case 9:
			LogicalOr();
			break;

		case 10:
			LogicalAnd();
			break;

		case 11:
			LogicalNot();
			break;

		case 12:
			LogicalXor();
			break;

		default:
			std::cout << "You have entered wrong option"<<std::endl;

		}
	std::cout << "Do you want to continue press 0 otherwise press any number to stop"<<std::endl;
	std::cin >> flag;
}
	return 0;
}
