#pragma once

#ifdef ADD_EXPORTS
#define ADD_API __declspec(dllexport)
#else
#define ADD_API __declspec(dllimport)
#endif

extern "C" ADD_API int addition(int number1, int number2);

