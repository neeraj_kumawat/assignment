#include <iostream>
using namespace std;
int sumofdigits(int number){
    int sum=0;
    while(number!=0)
    {
        sum=sum+number%10;
        number=number/10;
    }
    return sum;
}
int main()
{    int number;
    cout<<"Enter a number to find sum of digits";
    cin>>number;
       cout<<"Sum of digits of number "<<number <<" is "<<sumofdigits(number);
    return 0;
}
