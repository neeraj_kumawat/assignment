#include <iostream>
using namespace std;
int palindrome(int number){
    int reverse,num;
    num=number;
    while(num!=0){
        reverse=reverse*10+num%10;
        num=num/10;
    }
    if(reverse==number)
    return 1;
return 0;
}
int main()
{
    int number;
    cout<<"Enter a number to check that it is palindrome or not";
    cin>>number;
    if(palindrome(number))
    cout<<"Number is palindrome";
    else
    cout<<"Number is not palindrome";

    return 0;
}
