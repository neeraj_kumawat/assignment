#include<iostream>
using namespace std;
class Test {
public:
	Test() {
		cout << "Constructor is called";  //call when object is created
	}
	~Test() {
		cout << "Destructor is called ";   //call when scope of object is over
	}
};
int main() {
	
	Test object; 

	cout << "I am in middle of program";


}