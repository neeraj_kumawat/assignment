#include<iostream>
class LinearSearch {

public:
	int size, *elements;

	LinearSearch(int no_of_elements) {

		size = no_of_elements;
		elements = new int[size];
	}

	~LinearSearch() {

		delete [] elements;
	}

	void Input_Elements() {

		std::cout << "Enter the elements \n";
		for (int index = 0; index < size; index++)
		{
			std::cin >> elements[index];
		}

		
	}

	int Linear_Search(int number)
	{
		for (int index = 0; index < size; index++)
		{
			if (elements[index] == number)
				return 1;
		}
		return -1;
	}
};

int main() {

	int size,number;
	std::cout << "Enter the number of elements \n";
	std::cin >> size;
	
	if (size <= 0)
	{
		std::cout << "Enter a valid input";
	}
	else {

		LinearSearch object(size);

		object.Input_Elements();
		std::cout << "Enter the element that you want to find \n";
		std::cin >> number;

		if (object.Linear_Search(number) == 1)
			std::cout << "Number is found";
		else
			std::cout << "Number is not Found";
	}

	return 0;
}
