#include<iostream>
class ExponentialSearch {

public:
	int size, *elements;

	ExponentialSearch(int no_of_elements) {

		size = no_of_elements;
		elements = new int[size];
	}

	~ExponentialSearch() {

		delete [] elements;
	}

	void Input_Elements() {

		std::cout << "Enter the elements \n";
		for (int index = 0; index < size; index++)
		{
			std::cin >> elements[index];
		}
	}

	int BinarySearch(int elements[], int left_index, int right_index, int number)
	{
		int mid;
		if (left_index <= right_index)
		{
			mid = (left_index + right_index) / 2;

			if (elements[mid] == number)
				return mid;
			else if (number > elements[mid])
				left_index = mid + 1;
			else if (number < elements[mid])
				right_index = mid - 1;

		}
		return -1;
	}

	int Exponential_Search( int number)
	{

		if (elements[0] == number)
			return 0;

		int index = 1;
		while (index < size && elements[index] <= number)
			index = index * 2;

		int mimimum = index > (size - 1) ? size - 1 : index;

		return BinarySearch(elements, index / 2, mimimum, number);
	}
};
int main() {

	int size, number;
	std::cout << "Enter the number of elements \n";
	std::cin >> size;
	
	if (size <= 0)
	{
		std::cout << "Enter the valid input";
	}
	else {

		ExponentialSearch object(size);

		object.Input_Elements();
		std::cout << "Enter the number you want to search \n";
		std::cin >> number;

		int index = object.Exponential_Search(number);
		if (index != -1)
			std::cout << "number is found  at index " << index;
		else
			std::cout << "Number is not found ";
	}

	return 0;
}