#include<iostream>

class BinarySearch {

public:
	int size, *elements;

	BinarySearch(int no_of_elements) {

		size = no_of_elements;
		elements = new int[size];
	}

	~BinarySearch() {

		delete [] elements;
	}

	void Input_Elements() {

		std::cout << "Enter the elements \n";
		for (int index = 0; index < size; index++)
		{
			std::cin >> elements[index];
		}

	}

	int Binary_Search( int number)
	{
		int mid, start = 0, end = size - 1;

		while (start <= end) {

			mid = (start + end) / 2;

			if (elements[mid] == number)
				return mid;
			else if (number < elements[mid])
				end = mid - 1;
			else if (number > elements[mid])
				start = mid + 1;
		}
		return -1;
	}
};

int main() {

	int size,number;
	std::cout << "Enter the number of elements \n";
	std::cin >> size;
	
	if (size <= 0)
	{
		std::cout << "Enter the valid input";
	}
	else {

		BinarySearch object(size);

		object.Input_Elements();
		std::cout << "Enter the number you want to search \n";
		std::cin >> number;

		int index = object.Binary_Search( number );
		if (index != -1)
			std::cout << "number is found  at index " << index;
		else
			std::cout << "Number is not found ";
	}

	return 0;

}