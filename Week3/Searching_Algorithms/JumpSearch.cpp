#include<iostream>
#include<cmath>

class JumpSearch {

public:
	int size, *elements;

	JumpSearch(int no_of_elements) {

		size = no_of_elements;
		elements = new int[size];
	}

	~JumpSearch() {

		delete [] elements;
	}

	void Input_Elements() {

		std::cout << "Enter the elements \n";
		for (int index = 0; index < size; index++)
		{
			std::cin >> elements[index];
		}

	}

	int Jump_Search(int number)
	{

		int jumpsize = floor(sqrt(size));
		int prev = 0,minimum;

		while ((jumpsize < size) && (number > elements[jumpsize]))
		{
			prev = jumpsize;
			jumpsize = jumpsize + floor(sqrt(size));

			

		}

		minimum = jumpsize < (size - 1) ? jumpsize : size - 1;
	
		for (int index = prev; index <= minimum; index++)
		{
			
			if (elements[index] == number)
			{
				return index;
			}
		}

		return -1;
	}
};
int main() {

	int size, number;
	std::cout << "Enter the number of elements \n";
	std::cin >> size;
	
	if (size <= 0)
	{
		std::cout << "Enter a valid Input";
	}
	else
	{
		JumpSearch object(size);

		object.Input_Elements();
		std::cout << "Enter the element that you want to find \n";
		std::cin >> number;

		int index = object.Jump_Search(number);

		if (index != -1)
			std::cout << "Number is found at " << index;
		else
			std::cout << "Number is not Found";
	}

	return 0;
}