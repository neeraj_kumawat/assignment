#include<iostream>

class Stack {

public:

	struct Node {

		float number;
		struct Node *next;
	} *top;

	Stack() {

		top = NULL;
	}

	struct Node * CreateNode() {

		struct Node *node = (struct Node *)malloc(sizeof(struct Node));
		node->next = NULL;

		std::cout << "Enter the element to push \n";
		std::cin >> node->number;

		return node;

	}
	void Stack_Empty() {

		if (top == NULL)
		{
			std::cout << "Stack is empty \n";
		}
		else
		{
			std::cout << "Stack is not Empty \n";
		}

	}

	void Push() {

		struct Node *node = CreateNode();

		if (top == NULL)
			top = node;
		else {

			node->next = top;
			top = node;
		}	
	}

	void Pop() {

		if (top == NULL)
		{
			std::cout << "Stack is Empty \n";
		}
		else
		{
			struct Node *temp = top;

			top = top->next;

			delete temp;
		}
	}

	void Display() {

		if (top == NULL)
		{
			std::cout << "Stack is empty \n";
		}
		else {

			struct Node *temp = top;

			while (temp != NULL)
			{
				std::cout << temp->number << " ";
				temp = temp->next;
			}

			std::cout << std::endl;
		}
	}
};

int main() {

	int option, check_condition = 1;

	Stack object;

	while (check_condition == 1)
	{

	std::cout << "Enter the option to choose from Following operation \n";
	std::cout << "1.Check Stack is empty or not \n";
	std::cout << "2.To push the element \n";
	std::cout << "3.To pop the element \n ";
	std::cout << "4. To display the Stack element \n";
	std::cin >> option;

		switch (option)
		{
		case 1:
			object.Stack_Empty();
			break;

		case 2:
			object.Push();
			break;

		case 3:
			object.Pop();
			break;

		case 4:
			object.Display();
			break;

		default:
			std::cout << "Enter a valid input \n";
			break;
		}

		std::cout << "Do you want to continue press 1 otherwise press 0 \n";
		std::cin >> check_condition;
	}

	return 0;
}