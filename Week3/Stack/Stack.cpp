#include<iostream>

class Stack{

public:
	int top,size,*stack;

	Stack(int stack_size) {

		size = stack_size;
		top = -1;
		stack = new int[size];
	}
	~Stack() {

		delete stack;
	}

	int Stack_Empty() {

		if (top == -1)
			return 1;
		return 0;
	}
	int Stack_Full() {

		if (top == size - 1)
			return 1;
		return 0;

	}
	void Push(int element)
	{
		if (Stack_Full())
			std::cout << "Stack is overflow \n";
		else
		{
			top += 1;
			stack[top] = element;
		}
	}

	void Pop() {

		if (Stack_Empty())
			std::cout << "Stack is underflow \n";
		else
			top -= 1;
	}

	void Print_Elements() {

		if (Stack_Empty())
			std::cout << "Stack is Empty \n";
		else
		{
			for (int index = top; index >= 0; index--)
				std::cout << stack[index] << " ";
		}
	}
};

int main() {

	int option, size,element,condition_check=1;
	std::cout << "Enter the size of stack \n";
	std::cin >> size;

	if (size <= 0)
	{
		std::cout << "Enter a valid input";
	}
	else {

		Stack object(size);

		while (condition_check == 1) {

			std::cout << "Choose the operation you want to perform on Stack \n";
			std::cout << "1.Check the Stack is empty or not \n";
			std::cout << "2.Check the Stack is Full or not \n";
			std::cout << "3.To push a element \n";
			std::cout << "4.To pop a element \n";
			std::cout << "5.To print the elments of stack from top \n";
			std::cin >> option;

			switch (option) {

			case 1:
				if (object.Stack_Empty())
					std::cout << "Stack is empty \n";
				else
					std::cout << "Stack is not Empty \n";
				break;

			case 2:
				if (object.Stack_Full())
					std::cout << "Stack is Full \n";
				else
					std::cout << "Stack is not Full \n";
				break;

			case 3:
				std::cout << "Enter the element\n";
				std::cin >> element;
				object.Push(element);
				break;

			case 4:
				object.Pop();
				break;

			case 5:
				object.Print_Elements();
				break;

			default:
				std::cout << "You entered wrong input \n";
			}

			std::cout << "Do you want to continue press 1 otherwise press 0 \n";
			std::cin >> condition_check;
		}
	}
	return 0;
}