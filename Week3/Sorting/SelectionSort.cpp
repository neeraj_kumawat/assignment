#include<iostream>

class SelectionSort {

public:

	float *element;
	int size;

	SelectionSort(int array_size) {

		size = array_size;
		element = new float[size];
	}

	~SelectionSort() {

		delete[] element;
	}

	void Input() {

		std::cout << "Enter the elements \n";

		for (int index = 0; index < size; index++)
		{
			std::cin >> element[index];
		}
	}

	void Swap(float *number1, float *number2)
	{
		float temp;

		temp = *number1;
		*number1 = *number2;
		*number2 = temp;
	}

	void Selection_Sort() {

		float minimum;
		int position;

		for (int index1 = 0; index1 < size - 1; index1++)
		{
			minimum = element[index1];
			position = index1;

			for (int index2 = index1 + 1; index2 < size; index2++)
			{
				if ( element[index2]< minimum)
				{
					minimum = element[index2];
					position = index2;
				}
			}
			Swap(&element[index1],&element[position]);
	   }

		
	}

	void Display() {

		for (int index = 0; index < size; index++)
		{
			std::cout << element[index] << " ";
		}

		std::cout << std::endl;
	}
};

int main() {

	int size;

	std::cout << "Enter the size of array \n";
	std::cin >> size;

	if (size <= 0)
	{
		std::cout << "Enter a valid input \n";
	}
	else
	{
		SelectionSort object(size);

		object.Input();
		std::cout << "Before sorting elements are \n";
		object.Display();

		std::cout << "After sorting elements are \n";
		object.Selection_Sort();
		object.Display();
	}

	return 0;
}