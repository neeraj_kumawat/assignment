#include<iostream>

class MergeSort {

public:

	float *element;
	int size;

	MergeSort(int array_size) {

		size = array_size;
		element = new float[size];
	}

	~MergeSort() {

		delete[] element;
	}

	void Input() {

		std::cout << "Enter the elements \n";

		for (int index = 0; index < size; index++)
		{
			std::cin >> element[index];
		}
	}

	void Merge(int start,int mid,int end)
	{

		int length1, length2;

		length1 = mid - start + 1;
		length2 = end-mid;

		float *list1 = new float[length1];
		float *list2 =new float[length2];

		for (int index = 0; index < length1; index++)
			list1[index] = element[index+start];

		for (int index = 0; index < length2; index++)
			list2[index] = element[index+mid+1];

		int left = 0;
		int right =0;
		int index=start;

		while ((left < length1) && (right < length2))
		{

			if (list1[left] <= list2[right])
			{
				element[index] = list1[left];
				left++;
			}
			else
			{
				element[index] = list2[right];
					right++;
			}
			index++;
		}

		while (left < length1)
		{
			element[index] = list1[left];
			left++;
			index++;
		}

		while (right < length2)
		{
			element[index] = list2[right];
			right++;
			index++;
		}

		delete[] list1;
		delete[] list2;

	}
	void Merge_Sort(int start,int end) {

		int mid;

		if (start < end)
		{   
			mid = (start + end) / 2;

			Merge_Sort(start, mid);
			Merge_Sort(mid + 1, end);
			Merge(start, mid, end);
		}
	}

	void Display() {

		for (int index = 0; index < size; index++)
		{
			std::cout << element[index] << " ";
		}

		std::cout << std::endl;
	}
};

int main() {

	int size;

	std::cout << "Enter the size of array \n";
	std::cin >> size;

	if (size <= 0)
	{
		std::cout << "Enter a valid input \n";
	}
	else
	{
		MergeSort object(size);

		object.Input();
		std::cout << "Before sorting elements are \n";
		object.Display();

		std::cout << "After sorting elements are \n";
		object.Merge_Sort(0,size-1);
		object.Display();
	}

	return 0;
}