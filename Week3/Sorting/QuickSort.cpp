#include<iostream>

class QuickSort {

public:

	float *element;
	int size;

	QuickSort(int array_size) {

		size = array_size;
		element = new float[size];
	}

	~QuickSort() {

		delete[] element;
	}

	void Input() {

		std::cout << "Enter the elements \n";

		for (int index = 0; index < size; index++)
		{
			std::cin >> element[index];
		}
	}

	void Swap(float *number1, float *number2)
	{
		float temp;

		temp = *number1;
		*number1 = *number2;
		*number2 = temp;
	}

	int parititon( int start, int end)
	{
		int move = start;
		float pivot = element[end];

		for (int index = start; index < end ; index++)
		{
			if (element[index] <= pivot)
			{
				Swap(&element[index], &element[move]);
					move++;
			}
		}

		Swap(&element[move], &element[end]);

		return move;
	}

	void Quick_Sort(int start,int end) {

		int index;

		if (start < end)
		{
			index = parititon(start, end);

			Quick_Sort(start, index - 1);

			Quick_Sort(index + 1, end);

		}
	}

	void Display() {

		for (int index = 0; index < size; index++)
		{
			std::cout << element[index] << " ";
		}

		std::cout << std::endl;
	}
};

int main() {

	int size;

	std::cout << "Enter the size of array \n";
	std::cin >> size;

	if (size <= 0)
	{
		std::cout << "Enter a valid input \n";
	}
	else
	{
	    QuickSort object(size);

		object.Input();
		std::cout << "Before sorting elements are \n";
		object.Display();

		std::cout << "After sorting elements are \n";
		object.Quick_Sort(0,size-1);
		object.Display();
	}

	return 0;
}