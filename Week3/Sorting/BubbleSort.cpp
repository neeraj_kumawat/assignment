#include<iostream>

class BubbleSort {

public:

	float *element;
	int size;

	BubbleSort(int array_size) {

		size = array_size;
		element = new float[size];
	}

	~BubbleSort() {

		delete [] element;
	}

	void Input() {

		std::cout << "Enter the elements \n";

		for (int index = 0; index < size; index++)
		{
			std::cin >> element[index];
		}
	}

	void Swap(float *number1, float *number2)
	{
		float temp;

		temp = *number1;
		*number1 = *number2;
		*number2 = temp;
	}

	void Bubble_Sort() {

		int flag = 0;

		for (int pass = 1; pass < size; pass++)
		{
			for (int index = 0; index < size - pass; index++)
			{
				if (element[index] > element[index + 1])
				{
					Swap(&element[index], &element[index + 1]);
					flag = 1;
				}
			}
			if (flag == 0)
				break;
		}
	}

	void Display() {

		for (int index = 0; index < size; index++)
		{
			std::cout << element[index] << " ";
		}

		std::cout << std::endl;
	}
};

int main() {

	int size;

	std::cout << "Enter the size of array \n";
	std::cin >> size;

	if (size <= 0)
	{
		std::cout << "Enter a valid input \n";
	}
	else
	{
		BubbleSort object(size);

		object.Input();
		std::cout << "Before sorting elements are \n";
		object.Display();

		std::cout << "After sorting elements are \n";
		object.Bubble_Sort();
		object.Display();
	}

	return 0;
}