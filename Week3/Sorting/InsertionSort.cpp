#include<iostream>

class InsertionSort {

public:

	float *element;
	int size;

	InsertionSort(int array_size) {

		size = array_size;
		element = new float[size];
	}

	~InsertionSort() {

		delete[] element;
	}

	void Input() {

		std::cout << "Enter the elements \n";

		for (int index = 0; index < size; index++)
		{
			std::cin >> element[index];
		}
	}

	void Insertion_Sort() {

		int prev,temp;

		for (int index = 1; index < size; index++)
		{
			temp = element[index];
			prev = index - 1;

			while ((prev >= 0) && (element[prev] > temp))
			{
				element[prev + 1] = element[prev];
				prev--;
			}
			
			element[prev + 1] = temp;
     	}
		
	}

	void Display() {

		for (int index = 0; index < size; index++)
		{
			std::cout << element[index] << " ";
		}

		std::cout << std::endl;
	}
};

int main() {

	int size;

	std::cout << "Enter the size of array \n";
	std::cin >> size;

	if (size <= 0)
	{
		std::cout << "Enter a valid input \n";
	}
	else
	{
		InsertionSort object(size);

		object.Input();
		std::cout << "Before sorting elements are \n";
		object.Display();

		std::cout << "After sorting elements are \n";
		object.Insertion_Sort();
		object.Display();
	}

	return 0;
}