#include<iostream>

class CircularLinkedList {

public:

	struct Node {

		float element;
		struct Node *link;
	} *head,*tail;

	CircularLinkedList() {

		head = NULL;
		tail = NULL;
	}

	struct Node * Node_Input() {

		struct Node *node = (struct Node *)malloc(sizeof(struct Node));
		node->link = node;


		std::cout << "Enter the element \n";
		std::cin >> node->element;

		return node;

	}


	void Insert_At_Front()
	{
		struct Node *node = Node_Input();
		if ((head == NULL) && (tail==NULL))
		{
			head = node;
			tail = node;
		}
		else {
			node->link = head;
			head = node;
			tail->link = node;
		}
	}

	void Insert_At_End() {

		struct Node *node = Node_Input();

		if (head == NULL)
		{
			head = node;
			tail = node;
		}
		else
		{
			tail->link = node;
			node->link = head;
			tail = node;
		}

	}

	void Insert_After_ELement()
	{
		int number;
		std::cout << "Enter the number after which you want to insert \n";
		std::cin >> number;

		struct Node *node = Node_Input();

		if ((head == NULL) && (tail==NULL))
		{
			std::cout << "Linked list is Empty \n";
		}
		else
		{
			struct Node *temp = head;
			int flag = 0;

			while (temp->link != head)
			{
				if (temp->element == number)
				{
					flag = 1;
					break;
				}
				temp = temp->link;
			}
                
			if ((flag == 0) && (temp->element == number))
			{
				temp->link = node;
				node->link = head;
				tail = node;
				
			}
			else if (flag == 1)
			{
				node->link = temp->link;
				temp->link = node;
			}
			else
			{
				std::cout << "Number is not Found";
			}
		}

	}

	void Delete_At_Front() {

		struct Node *temp = head;

		if ((head == NULL) &&(tail==NULL))
		{
			std::cout << "Linked list is Empty \n";
		}
		else if (temp->link ==head)
		{
			head = NULL;
			tail = NULL;

			delete temp;
		}
		else{

			head = head->link;
			tail->link = head;
			delete temp;
		}
	}

	void Delete_At_End() {

		struct Node *temp = head;;

		if ((head == NULL) &&(tail==NULL))
		{
			std::cout << "Linked list is Empty \n";
		}
		else if (temp->link == head)
		{
			head = NULL;
			tail = NULL;

			delete temp;
		}
		else
		{
			while (temp->link->link != head)
			{
				temp = temp->link;
			 }
			temp->link = temp->link->link;
			tail = temp;
			
		}

	}

	void Delete_Element() {


		if ((head == NULL) &&(tail==NULL))
		{
			std::cout << "Linked list is Empty \n";
		}
		else {
			float number;
			int check = 0;

			std::cout << "Enter a element to Delete";
			std::cin >> number;

			struct Node *temp = head;
			if ((temp->element == number) && (temp->link != head))
			{
				head = head->link;
				tail->link = head;
				check = 1;

				delete temp;
			}
			else if ((temp->link == head) && (temp->element == number))
			{
				head = NULL;
				tail = NULL;
				check = 1;

				delete temp;

			}
			else {

				while (temp->link != head)
				{
					if (temp->link->element == number)
					{
						check = 1;
						break;

					}
					temp = temp->link;
				}

				if (check == 1)
				{
					struct Node *freenode;

					freenode = temp->link;
					temp->link = temp->link->link;

					if (temp->link == head)
						tail = temp;

					delete freenode;
				}
			}

			if (check == 0)
				std::cout << "Element is not there";
		}
	}

	void Print_Elements() {

		if ((head == NULL) && (tail==NULL))
		{
			std::cout << "Linked list is Empty \n";
		}
		else
		{
			struct Node *current = head;

			while (current->link != head)
			{
				std::cout << current->element << " ";

				current = current->link;
			}
			std::cout << current->element << " " << std::endl;
		}
	}
};

int main() {

	CircularLinkedList object;
	int option, check_condition = 1;

	while (check_condition == 1) {

		std::cout << "Enter the option \n";
		std::cout << "1.Insertion at front \n";
		std::cout << " 2.Insertion at End \n";
		std::cout << "3.Insertion after a element \n";
		std::cout << "4.Deletion from front \n";
		std::cout << "5.Deletion from End \n";
		std::cout << "6.Deletion of Element \n";
		std::cout << "7.Print the Linked List values \n";
		std::cin >> option;

		switch (option)
		{
		case 1:
			object.Insert_At_Front();
			break;

		case 2:
			object.Insert_At_End();
			break;

		case 3:
			object.Insert_After_ELement();
			break;

		case 4:
			object.Delete_At_Front();
			break;

		case 5:
			object.Delete_At_End();
			break;

		case 6:
			object.Delete_Element();
			break;

		case 7:
			object.Print_Elements();

		default:
			break;
		}

		std::cout << "Do you want to continue  press 1 otherwise press 0\n";
		std::cin >> check_condition;

	}
	return 0;
}