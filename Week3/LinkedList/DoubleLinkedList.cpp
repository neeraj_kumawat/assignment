#include<iostream>

class DoubleLinkedList {

public:

	struct Node {

		float element;
		struct Node *prev;
		struct Node *next;

	} *head;

	DoubleLinkedList() {

		head = NULL;
	}

	struct Node * Node_Input() {

		struct Node *node = (struct Node *)malloc(sizeof(struct Node));
		node->prev = NULL;
		node->next = NULL;

		std::cout << "Enter the element \n";
		std::cin >> node->element;

		return node;

	}


	void Insert_At_Front()
	{
		struct Node *node = Node_Input();

		if (head == NULL)
			head = node;
		else {
			node->next = head;
			head->prev = node;
		}
		 
	}

	void Insert_At_End() {

		struct Node *node = Node_Input();

		if (head == NULL)
			head = node;
		else
		{
			struct Node *temp = head;

			while (temp->next != NULL)
			{
				temp = temp->next;
			}

			temp->next = node;
			node->prev = temp;
		}

	}

	void Insert_After_ELement()
	{
		int number;
		std::cout << "Enter the number after which you want to insert \n";
		std::cin >> number;

		struct Node *node = Node_Input();

		if (head == NULL)
		{
			std::cout << "Linked list is Empty \n";
		}
		else
		{
			struct Node *temp = head;
			int flag = 0;

			while (temp != NULL) 
			{
				if (temp->element == number)
				{
					flag = 1;
					break;
				}
				temp = temp->next;
			}
			if (flag == 1) {

				node->next = temp->next;
				temp->next->prev = node;
				node->prev = temp;
				temp->next = node;
			}
			else
			{
				std::cout << "Element is not there";
			}
		}

	}

	void Delete_At_Front() {

		if (head == NULL)
		{
			std::cout << "Linked list is Empty \n";
		}
		else
		{
			struct Node *temp = head;

			head = head->next;

			delete temp;
		}
	}

	void Delete_At_End() {

		struct Node *temp = head;;

		if (head == NULL)
		{
			std::cout << "Linked list is Empty \n";
		}
		else if (temp->next == NULL)
		{
			head = NULL;

			delete temp;
		}
		else
		{

			while (temp->next != NULL)
			{
				temp = temp->next;
			}

			temp->prev->next = NULL;

			delete temp;
		}

	}

	void Delete_Element() {


		if (head == NULL)
		{
			std::cout << "Linked list is Empty \n";
		}
		else {
			float number;
			int flag = 0;

			std::cout << "Enter a number to Delete";
			std::cin >> number;

			struct Node *temp = head;

			if ((temp->next == NULL)&&(temp->element == number))
			{
					head = NULL;
					flag = 1;
					delete temp;
				
			}
			else if((temp->next!=NULL)&&(temp->element==number))
			{

				head = head->next;
				flag = 1;
				delete temp;
			}
			else
			{

				while (temp != NULL) 
				{
					if (temp->element == number)
					{
						flag = 1;
						break;
					}
					temp = temp->next;
				}

				if (flag == 1)
				{
					temp->prev->next = temp->next;

					if (temp->next != NULL)
					{
						temp->next->prev = temp->prev;
					}
					delete temp;
					
				}

				
			}

			if (flag == 0)
			{
				std::cout << "Element is not there";
			}
		}
	}

	void Print_Elements() {

		if (head == NULL)
		{
			std::cout << "Linked list is Empty \n";
		}
		else
		{
			struct Node *current = head;

			while (current != NULL)
			{
				std::cout << current->element << " ";

				current = current->next;
			}
		}
	}
};

int main() {

	DoubleLinkedList object;
	int option, check_condition = 1;

	while (check_condition == 1) {

		std::cout << "Enter the option \n";
		std::cout << "1.Insertion at front \n";
		std::cout << " 2.Insertion at End \n";
		std::cout << "3.Insertion after a element \n";
		std::cout << "4.Deletion from front \n";
		std::cout << "5.Deletion from End \n";
		std::cout << "6.Deletion of Element \n";
		std::cout << "7.Print the Linked List values \n";
		std::cin >> option;

		switch (option)
		{
		case 1:
			object.Insert_At_Front();
			break;

		case 2:
			object.Insert_At_End();
			break;

		case 3:
			object.Insert_After_ELement();
			break;

		case 4:
			object.Delete_At_Front();
			break;

		case 5:
			object.Delete_At_End();
			break;

		case 6:
			object.Delete_Element();
			break;

		case 7:
			object.Print_Elements();

		default:
			break;
		}

		std::cout << "Do you want to continue  press 1 otherwise press 0\n";
		std::cin >> check_condition;

	}
	return 0;
}