#include<iostream>

class SingleLinkedList {

public:

	struct Node {

		float element;
		struct Node *link;
	} *head;

	SingleLinkedList() {

		head = NULL;
	}

	struct Node * Node_Input() {

		struct Node *node = (struct Node *)malloc(sizeof(struct Node));
		node->link = NULL;

		
		std::cout << "Enter the element \n";
		std::cin >> node->element;
		
		return node;

	}


	void Insert_At_Front()
	{
		struct Node *node = Node_Input();
		if (head == NULL)
			head = node;
		else {
			node->link = head;
			head = node;
		}
	}

	void Insert_At_End() {
      
		struct Node *node = Node_Input();

		if (head == NULL)
			head = node;
		else
		{
			struct Node *temp = head;

			while (temp->link != NULL)
			{
				temp = temp->link;
			}

			temp->link = node;
		}

	}

	void Insert_After_ELement()
	{
		int number;
		std::cout << "Enter the number after which you want to insert \n";
		std::cin >>number;

		struct Node *node = Node_Input();

		if (head == NULL)
		{
			std::cout << "Linked list is Empty \n";
		}
		else
		{
			struct Node *temp = head;

			while ((temp!=NULL) && (temp->element != number))
			{
				temp = temp->link;
			}

			if (temp == NULL)
				std::cout << "Number is not Found \n";
			else
			{
				node->link = temp->link;
				temp->link = node;
			}
		}

	}

	void Delete_At_Front() {

		if (head == NULL)
		{
			std::cout << "Linked list is Empty \n";
		}
		else
		{
			struct Node *temp = head;

			head = head->link;

			delete temp;
		}
	}

	void Delete_At_End() {

		struct Node *temp = head;;

		if (head == NULL)
		{
			std::cout << "Linked list is Empty \n";
		}
		else if (temp->link == NULL)
		{
			head = NULL;

			delete temp;
		}
		else
		{

			while (temp->link->link != NULL)
			{
				temp = temp->link;
			}

			delete temp->link;

			temp->link = NULL;
		}

	}

	void Delete_Element() {


		if (head == NULL)
		{
			std::cout << "Linked list is Empty \n";
		}
		else {
			float element;
			int check = 0;

			std::cout << "Enter a element to Delete";
			std::cin >> element;

			struct Node *temp = head;
			if ((temp->element == element) && (temp->link != NULL))
			{
				head = head->link;
				check = 1;

				delete temp;
			}
			else if ((temp->link == NULL)&&(temp->element == element))
			{	
					head = NULL;
					check = 1;

					delete temp;
				
			}
			else {

				while (temp->link != NULL)
				{
					if (temp->link->element == element)
					{
						check = 1;
						break;
					}

					temp = temp->link;
				}
				
				if (check==1)
				{
					struct Node *freenode;

					freenode = temp->link;

					temp->link = temp->link->link;

					delete freenode;
				}

			}
			
			if (check == 0)
				std::cout << "Element is not there";
		}
	}

	void Print_Elements() {

		if (head == NULL)
		{
			std::cout << "Linked list is Empty \n";
		}
		else
		{
			struct Node *current = head;

			while (current != NULL)
			{
				std::cout << current->element << " ";

				current = current->link;
			}
		}
	}
};

int main() {

	SingleLinkedList object;
	int option,check_condition=1;
	
	while (check_condition == 1) {

		std::cout << "Enter the option \n";
		std::cout << "1.Insertion at front \n";
		std::cout << " 2.Insertion at End \n";
		std::cout << "3.Insertion after a element \n";
		std::cout << "4.Deletion from front \n";
		std::cout << "5.Deletion from End \n";
		std::cout << "6.Deletion of Element \n";
		std::cout << "7.Print the Linked List values \n";
		std::cin >> option;

		switch (option)
		{
		case 1:
			object.Insert_At_Front();
			break;

		case 2:
			object.Insert_At_End();
			break;

		case 3:
			object.Insert_After_ELement();
			break;

		case 4:
			object.Delete_At_Front();
			break;

		case 5:
			object.Delete_At_End();
			break;

		case 6:
			object.Delete_Element();
			break;

		case 7:
			object.Print_Elements();

		default:
			break;
		}

		std::cout << "Do you want to continue  press 1 otherwise press 0\n";
		std::cin >> check_condition;

	}
	return 0;
}