#include<iostream>

class CircularQueue {

public:
	int front, rear, *queue, size;

	CircularQueue(int circularqueue_size) {

		size = circularqueue_size;
		front = -1;
		rear = -1;
		queue = new int[size];
	}

	~CircularQueue() {

		delete queue;
	}

	int CircularQueue_Empty() {

		if ((front == -1) && (rear == -1))
			return 1;

		return 0;
		
	}

	int CircularQueue_Full() {

		if (((rear+1) % size)==front)
			return 1;

		return 0;
		
	}

	void Enqueue()
	{

		if (CircularQueue_Full())
			std::cout << "CircularQueue is Full \n";
		else
		{
			int element;

			std::cout << "Enter the element\n";
			std::cin >> element;

			if ((front == -1) && (rear == -1))
				front = rear = 0;
			else
			{
				rear = (rear + 1) % size;

			}
			queue[rear] = element;
			
		}
	}

	void Dequeue() {

		if (CircularQueue_Empty())
			std::cout << "CircularQueue is Empty \n";
		else
		{

			if (front == rear)
				front = rear = -1;
			else
			{
				front = (front + 1) % size;
			}
		}
	}

	void Print_Elements() {

		if (CircularQueue_Empty())
			std::cout << "CircularQueue is Empty \n";
		else
		{
			int index = front;

			while (index != rear) {
				std::cout << queue[index] << " ";
				index = (index + 1) % size;
			}

			std::cout << queue[index]<<std::endl;
		}
	}
};

int main() {

	int option, size, element, flag = 1;
	std::cout << "Enter the size of CircularQueue\n";
	std::cin >> size;

	if (size <= 0)
	{
		std::cout << "Enter a valid input \n";
	}
	else {

		CircularQueue object(size);

		while (flag == 1) {

			std::cout << "1.Check the CircularQueue is empty or not \n";
			std::cout << "2.Check the CircularQueue is Full or not \n";
			std::cout << "3.To Enqueue a element \n";
			std::cout << "4.To Dequeue a element \n";
			std::cout << "5.To print the elments of CircularQueue  \n";
			std::cin >> option;

			switch (option) {

			case 1:
				if (object.CircularQueue_Empty())
					std::cout << "CircularQueue is empty \n";
				else
					std::cout << "CircularQueue is not Empty \n";
				break;

			case 2:
				if (object.CircularQueue_Full())
					std::cout << "CircularQueue is Full \n";
				else
					std::cout << "CircularQueue is not Full \n";
				break;

			case 3:
				object.Enqueue();
				break;

			case 4:
				object.Dequeue();
				break;

			case 5:
				object.Print_Elements();
				break;

			default:
				std::cout << "You entered wrong input \n";
			}

			std::cout << "Do you want to continue press 1 otherwise press 0 \n";
			std::cin >> flag;
		}
	}

	return 0;
}