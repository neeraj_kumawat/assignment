#include<iostream>

class Queue {

public:
	int front, rear, *queue,size;

	Queue(int queue_size) {

		size = queue_size;
		front = -1;
		rear = -1;
		queue = new int[size];
	}

	~Queue() {

		delete queue;
	}

	int Queue_Empty() {

		if ((front == -1) && (rear == -1))
			return 1;
		
		return 0;
	}

	int Queue_Full() {

		if (rear == size - 1)
			return 1;

		return 0;
	}

	void Enqueue()
	{
		
		if (Queue_Full())
			std::cout << "Queue is Full";
		else
		{
			int element;

			std::cout << "Enter the element\n";
			std::cin >> element;

			if ((front == -1) && (rear == -1))
				front = rear = 0;
			else {

				rear++;
			}
				queue[rear] = element;
		}
	}

	void Dequeue() {

		if (Queue_Empty())
			std::cout << "Queue is Empty \n";
		else
		{
			if (rear == front)
				front = rear = -1;
			else {

				front++;
			}
		}
	}

	void Print_Elements() {

		if (Queue_Empty())
			std::cout << "Queue is Empty \n";
		else
		{
			
			for (int index = front; index <= rear; index++)
				std::cout << queue[index] << " ";
		}
	}
};

int main() {

	int option, size, element, condition_check = 1;
	std::cout << "Enter the size of queue\n";
	std::cin >> size;

	if (size <= 0)
	{
		std::cout << "Enter a valid input";
	}
	else {

		Queue object(size);

		while (condition_check == 1) {

			std::cout << "Choose the operation you want to perform on Queue \n";
			std::cout << "1.Check the Queue is empty or not \n";
			std::cout << "2.Check the Queue is Full or not \n";
			std::cout << "3.To Enqueue a element \n";
			std::cout << "4.To Dequeue a element \n";
			std::cout << "5.To print the elments of Queue  \n";
			std::cin >> option;

			switch (option) {

			case 1:
				if (object.Queue_Empty())
					std::cout << "Queue is empty \n";
				else
					std::cout << "Queue is not Empty \n";
				break;

			case 2:
				if (object.Queue_Full())
					std::cout << "Queue is Full \n";
				else
					std::cout << "Queue is not Full \n";
				break;

			case 3:
				object.Enqueue();
				break;

			case 4:
				object.Dequeue();
				break;

			case 5:
				object.Print_Elements();
				break;

			default:
				std::cout << "You entered wrong input \n";
			}

			std::cout << "Do you want to continue press 1 otherwise press 0 \n";
			std::cin >> condition_check;
		}
	}
	return 0;
}