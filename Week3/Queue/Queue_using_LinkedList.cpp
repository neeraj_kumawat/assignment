#include<iostream>

class Queue {

public:

	struct Node {

		float number;
		struct Node *next;
	} *front, *rear;

	Queue() {

		front = NULL;
		rear = NULL;
	}

	struct Node * CreateNode() {

		struct Node *node = (struct Node *)malloc(sizeof(struct Node));
		node->next = NULL;

		std::cout << "Enter the element  \n";
		std::cin >> node->number;

		return node;

	}
	void Queue_Empty() {

		if ((front == NULL) &&(rear==NULL))
		{
			std::cout << "Queue is empty \n";
		}
		else
		{
			std::cout << "Queue is not Empty \n";
		}

	}

	void Enqueue() {

		struct Node *node = CreateNode();

		if ((front == NULL) && (rear == NULL))
		{
			front = node;
			rear = node;
		}
		else {

			rear->next = node;
			rear = node;
		}
	}

	void Dequeue() {

		if ((front == NULL) && (rear==NULL))
		{
			std::cout << "Queue is Empty \n";
		}
		else if (front->next == NULL)
		{
			front = rear = NULL;
		}
		else
		{
			struct Node *temp = front;

			front = front->next;

			delete temp;
		}
	}

	void Display() {

		if ( (front == NULL) && (rear==NULL) )
		{
			std::cout << "Queue is empty \n";
		}
		else {

			struct Node *temp = front;

			while (temp != NULL)
			{
				std::cout << temp->number << " ";
				temp = temp->next;
			}

			std::cout << std::endl;
		}
	}
};

int main() {

	int option, check_condition = 1;

	Queue object;

	while (check_condition == 1)
	{

		std::cout << "Enter the option to choose from Following operation \n";
		std::cout << "1.Check Queue is empty or not \n";
		std::cout << "2.To enqueue the element \n";
		std::cout << "3.To dequeue the element \n ";
		std::cout << "4. To display the Queue elements \n";
		std::cin >> option;

		switch (option)
		{
		case 1:
			object.Queue_Empty();
			break;

		case 2:
			object.Enqueue();
			break;

		case 3:
			object.Dequeue();
			break;

		case 4:
			object.Display();
			break;

		default:
			std::cout << "Enter a valid input \n";
			break;
		}

		std::cout << "Do you want to continue press 1 otherwise press 0 \n";
		std::cin >> check_condition;
	}

	return 0;
}